import java.io.Serializable;

public class Record implements Serializable {
    private String name;
    private int value;
    private String date;
    private boolean type;

    public Record(String name,int value,String date,boolean type){
        this.name = name;
        this.value = value;
        this.date = date;
        this.type = type;
    }

    public String getName(){
        return this.name;
    }
}