import java.io.*;
import java.util.ArrayList;

public class FileManager {
    private static final String NOTES_PATH = "./records/";
    static {
        boolean isSuccessful = new File(NOTES_PATH).mkdirs();
        System.out.println("Creating " + NOTES_PATH + " directory is successful: " + isSuccessful);
    }
    public static File[] getFilesInDirectory() {
        return new File(NOTES_PATH).listFiles();
    }

    public static void fileWriter(RecordList recordList) {
        try{
            FileOutputStream fs=new FileOutputStream("./records/");
            ObjectOutputStream os=new ObjectOutputStream(fs);
            os.writeObject(recordList);
            fs.close();
            os.close();
        }catch(IOException e){
            System.out.println("error in writing the file");
        }
    }
    public static ArrayList<Record> fileReader(File file) {
        try{
            FileInputStream is = new FileInputStream(file);
            ObjectInputStream ois =new ObjectInputStream(is);
            RecordList recordList=(RecordList) ois.readObject();
            return recordList.getRecordList();
        }
        catch(IOException e){
            return null;
        }
        catch(ClassNotFoundException e){
            return null;
        }

    }

}
