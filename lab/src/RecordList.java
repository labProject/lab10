import java.io.Serializable;
import java.util.ArrayList;

public class RecordList implements Serializable {
    private ArrayList<Record> recordList;
    private String title;

    public RecordList(String title) {
        this.recordList = new ArrayList<>();
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void addRecord(Record record){
        recordList.add(record);
    }

    public  ArrayList<Record> getRecordList(){
        return this.recordList;
    }
}
